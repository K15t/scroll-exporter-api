# Description

This repository contains the public Java APIs for [Scroll HTML Exporter](https://marketplace.atlassian.com/plugins/com.k15t.scroll.scroll-html), [Scroll PDF Exporter](https://marketplace.atlassian.com/plugins/com.k15t.scroll.scroll-pdf) and [Scroll Word Exporter](https://marketplace.atlassian.com/plugins/com.k15t.scroll.scroll-office).

Binary, Javadoc and source artifacts are published through our Maven repository. See below for instructions how to access them.

The API project itself uses [semantic versioning](https://semver.org), the exporter apps however do not.

# License
The Scroll Exporter public APIs are licensed under the terms of the *MIT license*.

# Usage

The public APIs can be used by adding a `provided` Maven dependency to the project. At runtime this will be provided by the respective exporter app.

First you'll need to add the K15t Maven repository to your project or Maven settings:
```xml
<repositories>
    <repository>
        <id>k15t</id>
        <url>https://nexus.k15t.com/content/repositories/releases</url>
    </repository>
</repositories>
```

Then add these dependencies (pick which you need) in your app's `pom.xml`:
```xml
<dependencies>
    ...

    <dependency>
        <groupId>com.k15t.scroll</groupId>
        <artifactId>scroll-html-exporter-java-api</artifactId>
        <version>1.2.0</version>
        <scope>provided</scope>
    </dependency>
    
    <dependency>
        <groupId>com.k15t.scroll</groupId>
        <artifactId>scroll-pdf-exporter-java-api</artifactId>
        <version>1.2.0</version>
        <scope>provided</scope>
    </dependency>

    <dependency>
        <groupId>com.k15t.scroll</groupId>
        <artifactId>scroll-word-exporter-java-api</artifactId>
        <version>1.2.0</version>
        <scope>provided</scope>
    </dependency>

    ...
</dependencies>
```

Finally add the following OSGi imports to your `maven-confluence-plugin` configuration in `pom.xml` (again pick only the imports for those exporters you need to work with):
```xml
<plugin>
    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>maven-confluence-plugin</artifactId>
    <configuration>
        ...
        <instructions>
            <Import-Package>
                com.k15t.scroll.exporter.api.html;version="[1.0.0,2.0.0)",
                com.k15t.scroll.exporter.api.pdf;version="[1.0.0,2.0.0)",
                com.k15t.scroll.exporter.api.word;version="[1.0.0,2.0.0)",
                ...
            </Import-Package>
            ...
        </instructions>
        ...
    </configuration>
</plugin>
```
The version range `[1.0.0,2.0.0)` declares that your plugin is compatible with all versions of the API starting from (and including) version 1.0.0 up to (but NOT including) 2.0.0.

A new major version of the API will have breaking changes, so it is very probable that you'll need to adapt your source code. Therefore you should not assume that your code will work out of the box with the next major version of the API.

We recommend to declare the version range as follows:

* Use the version of the maven dependency as the lower bound
* Use the next major version as the **excluded** upper bound

## API Usage Examples
The following examples demonstrate how to export using Scroll PDF Exporter. Using other exporter APIs works in a similar way, for example use `WordExportService` instead of `PdfExportService` to perform an export with Scroll Word Exporter.
```java
public class ApiExamples {

    @Autowired @ComponentImport private PdfExportService pdfExportService;
    @Autowired @ComponentImport private PdfTemplateService pdfTemplateService;


    public void exportWithDescendantPages(long rootPageId) throws IOException {
        PdfExportRequest exportRequest = pdfExportService.createExportRequest(rootPageId);
        PdfExportResult exportResult = pdfExportService.export(exportRequest);

        saveExportResultToDisk(exportResult);
    }


    public void exportSinglePage(long rootPageId) throws IOException {
        PdfExportRequest exportRequest = pdfExportService.createExportRequest(rootPageId)
                .withExportScope(PdfExportRequest.EXPORT_SCOPE_CURRENT);
        PdfExportResult exportResult = pdfExportService.export(exportRequest);

        saveExportResultToDisk(exportResult);
    }


    public void exportWithCustomGlobalTemplate(long rootPageId, String templateName) throws IOException {
        String templateId = pdfTemplateService.getTemplates(null).stream()
                .filter(template -> (template.getName().contains(templateName)))
                .findFirst()
                .map(PdfTemplate::getId)
                .orElseThrow(() -> new IllegalStateException("Couldn't find template."));

        PdfExportRequest exportRequest = pdfExportService.createExportRequest(rootPageId)
                .withTemplateId(templateId);
        PdfExportResult exportResult = pdfExportService.export(exportRequest);

        saveExportResultToDisk(exportResult);
    }


    public void exportSingleLanguage(long rootPageId, String languageKey) throws IOException {
        PdfExportRequest exportRequest = pdfExportService.createExportRequest(rootPageId)
                .withLanguageKey(languageKey);
        PdfExportResult exportResult = pdfExportService.export(exportRequest);

        saveExportResultToDisk(exportResult);
    }


    private void saveExportResultToDisk(PdfExportResult exportResult) throws IOException {
        Path tempFile = Paths.get(System.getProperty("java.io.tmpdir"), exportResult.getFilename());
        try (InputStream in = exportResult.getExportFileData()) {
            Files.copy(in, tempFile);
        }

        if (exportResult.isSuccessful()) {
            System.out.println("PDF stored at: " + tempFile.toString());
        } else {
            System.out.println("error.zip stored at: " + tempFile.toString());
        }
    }

}
```
