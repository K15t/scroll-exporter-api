package com.k15t.scroll.exporter.api.html;

/**
 * Performs exports with Scroll HTML Exporter.
 */
public interface HtmlExportService {

    /**
     * Creates and pre-configures an export request that may be further customized and then used with {@link #export(HtmlExportRequest)}
     * in order to perform an export.
     *
     * @param rootPageId The ID of the Confluence page to start the export from.
     * @return A new {@link HtmlExportRequest} configured to perform exports of the specified root page and all its descendants, using a
     * bundled template.
     */
    HtmlExportRequest createExportRequest(long rootPageId);


    /**
     * Performs an export with the specified configuration.
     *
     * @param exportRequest A {@link HtmlExportRequest} containing the configuration of the export.
     * @return A {@link HtmlExportResult} containing information about and access to the results of the export.
     * @throws IllegalArgumentException If the validation of the specified export request detects any invalid configuration.
     * @see #createExportRequest(long)
     */
    HtmlExportResult export(HtmlExportRequest exportRequest) throws IllegalArgumentException;

}
