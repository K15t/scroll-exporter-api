package com.k15t.scroll.exporter.api.html;

import javax.annotation.Nullable;

import java.util.List;


/**
 * Provides access to export templates.
 */
public interface HtmlTemplateService {

    /**
     * Returns templates available to the specified space.
     *
     * @param spaceKey Optional. If specified the returned list contains both global templates and templates belonging to the specified
     * space. Otherwise only global themes are returned.
     * @return A list of {@link HtmlTemplate}s.
     */
    List<HtmlTemplate> getTemplates(@Nullable String spaceKey);

}
