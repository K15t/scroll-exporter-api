package com.k15t.scroll.exporter.api.html;

/**
 * Represents the request to perform an export with a specific configuration.
 * <p>
 * Instances of this interface are typically immutable and can be reused for multiple exports. The {@code with...} methods must be used
 * to change individual export parameters.
 * </p>
 *
 * @see HtmlExportService#createExportRequest(long)
 * @see HtmlExportService#export(HtmlExportRequest)
 */
public interface HtmlExportRequest {

    /**
     * Export scope that includes only the specified root page in exports.
     */
    String EXPORT_SCOPE_CURRENT = "current";

    /**
     * Export scope that includes the specified root page and all of its descendant pages in exports.
     */
    String EXPORT_SCOPE_DESCENDANTS = "descendants";

    /**
     * Export scope that includes all pages of the Scroll Documents document that the specified root page belongs to. Please note that
     * the specified root page must be the root of the document.
     */
    String EXPORT_SCOPE_DOCUMENT = "document";


    /**
     * @return A copy of this export request configured to start exports from the given Confluence page. Clients don't need to call this
     * if the same root page ID has already been specified when creating this export request.
     */
    HtmlExportRequest withRootPageId(long rootPageId);


    /**
     * @return A copy of this export request configured to use the specified export template. Please note that this template needs to be
     * available to the space of the export root page.
     * @see HtmlTemplateService
     */
    HtmlExportRequest withTemplateId(String templateId);


    /**
     * @return A copy of this export request configured to use the specified export scope. Export scopes determine what pages will be
     * included in the exported document. Use one of the constants in this interface.
     * @see #EXPORT_SCOPE_CURRENT
     * @see #EXPORT_SCOPE_DESCENDANTS
     * @see #EXPORT_SCOPE_DOCUMENT
     */
    HtmlExportRequest withExportScope(String exportScope);


    /**
     * @return A copy of this export request configured to export content of the specified Scroll Versions version.
     */
    HtmlExportRequest withVersionId(String versionId);


    /**
     * @return A copy of this export request configured to export content of the specified Scroll Versions variant.
     */
    HtmlExportRequest withVariantId(String variantId);


    /**
     * @return A copy of this export request configured to export content of the specified Scroll Translations language.
     */
    HtmlExportRequest withLanguageKey(String languageKey);


    /**
     * @return The page to start the export from.
     */
    long getRootPageId();


    /**
     * @return The ID of the template to use for the export.
     */
    String getTemplateId();


    /**
     * @return The export scope to use for the export.
     */
    String getExportScope();


    /**
     * @return The ID of the Scroll Versions version to export.
     */
    String getVersionId();


    /**
     * @return The ID of the Scroll Versions variant to export.
     */
    String getVariantId();


    /**
     * @return The key of the Scroll Translations language to export.
     */
    String getLanguageKey();

}
