package com.k15t.scroll.exporter.api.word;

import javax.annotation.Nullable;

import java.util.List;


/**
 * Provides access to export templates.
 */
public interface WordTemplateService {

    /**
     * Returns templates available to the specified space.
     *
     * @param spaceKey Optional. If specified the returned list contains both global templates and templates belonging to the specified
     * space. Otherwise only global themes are returned.
     * @return A list of {@link WordTemplate}s.
     */
    List<WordTemplate> getTemplates(@Nullable String spaceKey);

}
