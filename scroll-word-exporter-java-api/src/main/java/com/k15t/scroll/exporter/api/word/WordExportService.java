package com.k15t.scroll.exporter.api.word;

/**
 * Performs exports with Scroll Word Exporter.
 */
public interface WordExportService {

    /**
     * Creates and pre-configures an export request that may be further customized and then used with {@link #export(WordExportRequest)}
     * in order to perform an export.
     *
     * @param rootPageId The ID of the Confluence page to start the export from.
     * @return A new {@link WordExportRequest} configured to perform exports of the specified root page and all its descendants, using a
     * bundled template.
     */
    WordExportRequest createExportRequest(long rootPageId);


    /**
     * Performs an export with the specified configuration.
     *
     * @param exportRequest A {@link WordExportRequest} containing the configuration of the export.
     * @return A {@link WordExportResult} containing information about and access to the results of the export.
     * @throws IllegalArgumentException If the validation of the specified export request detects any invalid configuration.
     * @see #createExportRequest(long)
     */
    WordExportResult export(WordExportRequest exportRequest) throws IllegalArgumentException;

}
