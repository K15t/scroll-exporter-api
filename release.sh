#!/usr/bin/env bash

VERSION=""
if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    VERSION="$1"
else
    echo "Create a maven release with the specified version number.
No changes will be pushed to the remote repository until the end of the build process.

This project uses semantic versioning, see https://semver.org

Usage:
    $0 <MAJOR.MINOR.PATCH>

Examples:
    $0 3.2.15
    $0 3.1.0
    $0 1.0.0
"

    exit 0
fi


MVN_COMMAND=""
if [ -x "$(command -v mvn)" ]; then
    MVN_COMMAND="mvn"
elif [ -x "$(command -v atlas-mvn)" ]; then
    MVN_COMMAND="atlas-mvn"
else
    echo "Could find neither 'mvn' nor 'atlas-mvn'. Make sure one of them is on your path."
    exit 0
fi


GIT_COMMAND=""
if [ -x "$(command -v git)" ]; then
    GIT_COMMAND="git"
else
    echo "Could not find 'git'. Make sure it is on your path."
    exit 0
fi


${MVN_COMMAND} clean release:clean release:prepare release:perform -DreleaseVersion=${VERSION} --batch-mode && \
${GIT_COMMAND} push --tags && \
${GIT_COMMAND} push
