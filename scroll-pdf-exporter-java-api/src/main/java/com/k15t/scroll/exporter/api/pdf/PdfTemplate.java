package com.k15t.scroll.exporter.api.pdf;

import java.util.Optional;


/**
 * Represents a Scroll PDF Exporter template to be used for exports. Templates can be global or scoped to a space. Global templates can
 * be used for exports from all spaces. Templates scoped to a certain space can only be used in that space.
 */
public interface PdfTemplate {

    /**
     * @return A unique ID identifying this template.
     */
    String getId();


    /**
     * @return The name of the template as displayed in the user interface.
     */
    String getName();


    /**
     * @return An {@link Optional} containing the key of the space this template belongs to. Empty for global templates.
     */
    Optional<String> getSpaceKey();


    /**
     * @return {@code true} if this template is bundled by Scroll PDF Exporter (rather than created by the user), else {@code false}.
     */
    boolean isBundled();


    /**
     * @return The preferred page set (export scope) of the template. Template authors can set a preferred page set in the template
     * editor (Template Settings -> Export default Settings -> Select the default export scope for your plugin). By default, no preferred
     * page set is defined, and an empty Optional is returned. Otherwise, it is one of the EXPORT_SCOPE constants:
     * @see PdfExportRequest#EXPORT_SCOPE_CURRENT
     * @see PdfExportRequest#EXPORT_SCOPE_DESCENDANTS
     * @see PdfExportRequest#EXPORT_SCOPE_DOCUMENT
     */
    Optional<String> getPreferredPageSet();

}
