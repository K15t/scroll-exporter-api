package com.k15t.scroll.exporter.api.pdf;

import java.io.InputStream;
import java.util.Optional;


/**
 * Contains information about a finished export.
 */
public interface PdfExportResult {


    /**
     * @return A unique ID identifying this export.
     */
    String getExportJobId();


    /**
     * @return {@code true} if this export was successful, else {@code false}.
     */
    boolean isSuccessful();


    /**
     * @return The filename (without paths) that has been generated for the exported file. Callers may use this when storing the export
     * result, but it may be safely ignored if another name should be used.
     * <p>
     * The returned filename depends on the outcome of the export (see {@link #isSuccessful()}). If the export failed this method returns
     * the name of the error file (error.zip). Therefore callers should not make assumptions on the file extension without checking if the
     * export was successful.
     * </p>
     */
    String getFilename();


    /**
     * @return An {@link InputStream} providing access to the result of the export. Callers should evaluate {@link #isSuccessful()}
     * before making any assumptions on the content of this stream. If the export failed this will contain the data of the error file
     * rather than the expected exported document.
     */
    InputStream getExportFileData();


    /**
     * @return In case of an export error (see {@link #isSuccessful()}) this will contain details on the error that caused the export to
     * fail, otherwise an empty {@link Optional}.
     * <p>
     * The content of this message will most probably contain a Java stacktrace and therefore
     * it should be considered as technical information which might not be suitable to be displayed on user interfaces.
     * </p>
     */
    Optional<String> getErrorDetails();

}
