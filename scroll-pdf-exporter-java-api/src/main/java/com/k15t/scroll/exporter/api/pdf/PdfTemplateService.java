package com.k15t.scroll.exporter.api.pdf;

import javax.annotation.Nullable;

import java.util.List;


/**
 * Provides access to export templates.
 */
public interface PdfTemplateService {

    /**
     * Returns templates available to the specified space.
     *
     * @param spaceKey Optional. If specified the returned list contains both global templates and templates belonging to the specified
     * space. Otherwise only global themes are returned.
     * @return A list of {@link PdfTemplate}s.
     */
    List<PdfTemplate> getTemplates(@Nullable String spaceKey);

}
