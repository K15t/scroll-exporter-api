package com.k15t.scroll.exporter.api.pdf;

/**
 * Performs exports with Scroll PDF Exporter.
 */
public interface PdfExportService {

    /**
     * Creates and pre-configures an export request that may be further customized and then used with {@link #export(PdfExportRequest)}
     * in order to perform an export.
     *
     * @param rootPageId The ID of the Confluence page to start the export from.
     * @return A new {@link PdfExportRequest} configured to perform exports of the specified root page and all its descendants, using a
     * bundled template.
     */
    PdfExportRequest createExportRequest(long rootPageId);


    /**
     * Performs an export with the specified configuration.
     *
     * @param exportRequest A {@link PdfExportRequest} containing the configuration of the export.
     * @return A {@link PdfExportResult} containing information about and access to the results of the export.
     * @throws IllegalArgumentException If the validation of the specified export request detects any invalid configuration.
     * @see #createExportRequest(long)
     */
    PdfExportResult export(PdfExportRequest exportRequest) throws IllegalArgumentException;

}
